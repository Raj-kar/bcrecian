export const getDay = () => {
  const date = new Date();
  //   return 7, if it's sunday, bcoz in python sunday represents 0
  return date.getDay() == 0 ? 7 : date.getDay();
};

export const dateToLocalDate = (userDate) => {
  const current = new Date();
  const date = new Date(current.getFullYear(), current.getMonth(), userDate);

  var options = {
    weekday: "long",
    year: "numeric",
    month: "long",
    day: "numeric",
  };
  return date.toLocaleDateString("en-US", options);
};
