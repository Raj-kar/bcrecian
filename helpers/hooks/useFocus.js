import { useEffect, useState } from "react";

const useFocus = () => {
  const [isFocus, setIsFocus] = useState(null);

  // User has switched back to the tab
  const onFocus = () => {
    setIsFocus(true);
  };

  // User has switched away from the tab (AKA tab is hidden)
  const onBlur = () => {
    setIsFocus(false);
  };

  useEffect(() => {
    window.addEventListener("focus", onFocus);
    window.addEventListener("blur", onBlur);
    // Specify how to clean up after this effect
    return () => {
      window.removeEventListener("focus", onFocus);
      window.removeEventListener("blur", onBlur);
    };
  });

  return isFocus;
};

export default useFocus;
