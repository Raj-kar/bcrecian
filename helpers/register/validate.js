export const validatePassword = (password) => {
  let re;
  if (password.length < 8)
    return "Mininum eight character required for password.";
  re = /(.*[a-z].*)/;
  if (!re.test(password)) return "Minimum one lower case character required.";
  re = /(.*[A-Z].*)/;
  if (!re.test(password)) return "Minimum one uppper case character required.";
  re = /(.*\d.*)/;
  if (!re.test(password)) return "Minimum one digit required.";
  re = /(.*\W.*)/;
  if (!re.test(password)) return "Minimum one special character required.";

  return false;
};

export const validateData = (name, roll, email, password) => {
  // Validate NAME
  if (name.length < 2) return "Mininum two character required for name.";
  let re = /^[a-zA-Z ]{2,30}$/;
  if (!re.test(String(name))) return "Name is not valid.";

  //   Validate Roll no
  re = /^[1-9]\d{10,10}$/;
  if (!re.test(roll)) return "Roll is not valid.";

  //   Validate Email
  re =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (!re.test(String(email).toLowerCase())) return "Email is not valid.";

  //   Validate Password
  return validatePassword(password);
};
