importScripts("https://www.gstatic.com/firebasejs/8.2.0/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/8.2.0/firebase-messaging.js");
var firebaseConfig = {
    apiKey: "AIzaSyDC94VsmZh0YWkcrdeR_s8WZtg712DP5Bw",
    authDomain: "django-notification-b0084.firebaseapp.com",
    projectId: "django-notification-b0084",
    storageBucket: "django-notification-b0084.appspot.com",
    messagingSenderId: "608576668692",
    appId: "1:608576668692:web:f6a710d7d3346ec8e1d301",
    measurementId: "G-Y98CRFQ6Z6"
};
firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function (payload) {
    console.log(payload);
    const notification = JSON.parse(payload);
    const notificationOption = {
        body: notification.body,
        icon: notification.icon
    };
    return self.registration.showNotification(payload.notification.title, notificationOption);
});