import { Fragment, useState } from "react";
import Modal from "./Modal";

const ModalConfig = ({ buttonText, Component, styles }) => {
  const [isShow, setIsShow] = useState(false);

  const openModal = () => {
    setIsShow(true);
  };

  const closeModal = () => {
    setIsShow(false);
  };

  return (
    <Fragment>
      <button id="modalBtn" onClick={openModal} className={styles}>
        {buttonText}
      </button>
      {isShow && (
        <Modal closeModal={closeModal} isOpen={isShow}>
          <Component closeModal={closeModal} />
        </Modal>
      )}
    </Fragment>
  );
};

export default ModalConfig;
