import ModalConfig from "../Modal/ModalConfig";
import LoginForm from "../Form/LoginForm";
import RegisterForm from "../Form/RegisterForm";
import Download from "./Download";

import styles from "./Home.module.scss";
import FormControl from "../Form/FormControl";

const Home = () => {
  return (
    <div className={styles.container}>
      <h1 className={styles.heading}>Welcome to, College Space.</h1>
      <p>
        Here, you can download notes, check your daily class routine within a
        single click, look through noties and many more.
      </p>
      <p className={styles.heading2}>
        To get started, you have to Login / SignUp.
      </p>
      <div className={styles.buttons}>
        <ModalConfig
          buttonText="Register"
          Component={RegisterForm}
          styles={styles.register}
        />
        <ModalConfig
          buttonText="Login"
          Component={FormControl}
          styles={styles.login}
        />
      </div>
      <div className={styles.promotion}>
        <p className={styles.heading2}>
          To get notifications and stay upto date, allow notifications &
          download our app.
        </p>
        <ModalConfig
          buttonText="Instructions"
          Component={Download}
          styles={styles.downloadBtn}
        />
      </div>
    </div>
  );
};

export default Home;
