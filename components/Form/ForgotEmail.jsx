import { Fragment, useRef, useState } from "react";
import { MdVerifiedUser } from "react-icons/md";
import useAPI from "../../helpers/hooks/useAPI";
import Spinner from "../Spinner/Spinner";
import InputBox from "./InputBox";

import styles from "./Form.module.scss";
import { ACTIONS } from "./formReducer";

const ForgotEmail = ({ closeModal, formDispatch }) => {
  const { isLoading, error, sendRequest } = useAPI();
  const [email, setEmail] = useState(null);
  const rollRef = useRef(null);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const roll = rollRef.current.value;

    const requestConfig = {
      url: "/api/auth/searchaccount",
      method: "POST",
      headers: { "content-type": "application/json" },
      body: {
        roll,
      },
    };

    const data = await sendRequest(requestConfig);
    setEmail(data?.message);
  };

  const Options = () => (
    <div className="extras">
      <p onClick={() => formDispatch({ type: ACTIONS.Login })}>Login</p>
      <p onClick={() => formDispatch({ type: ACTIONS.ForgotPassword })}>
        Forgot Password
      </p>
    </div>
  );

  return (
    <Fragment>
      <p className="modalHeading">Forgot Email</p>
      {email ? (
        <>
          <p className={styles.form}>Your email is - {email}</p>
          <Options />
        </>
      ) : (
        <form className={styles.form} onSubmit={handleSubmit}>
          <InputBox
            id="user_roll"
            Icon={MdVerifiedUser}
            title="Enter your Roll number"
            type="text"
            inputRef={rollRef}
          />
          {error && <p className={styles.errText}>{error}</p>}
          {isLoading ? <Spinner /> : <button>Search Account</button>}
        </form>
      )}
      {!isLoading && <Options />}
    </Fragment>
  );
};

export default ForgotEmail;
