import { Fragment, useRef } from "react";
import { useRouter } from "next/router";
import { userActions } from "../../store/user";
import { useDispatch } from "react-redux";
import { MdEmail } from "react-icons/md";
import { RiLockPasswordFill } from "react-icons/ri";
import useAPI from "../../helpers/hooks/useAPI";
import Spinner from "../Spinner/Spinner";
import InputBox from "./InputBox";

import styles from "./Form.module.scss";
import { ACTIONS } from "./formReducer";

const LoginForm = ({ closeModal, formDispatch }) => {
  const { isLoading, error, sendRequest } = useAPI();
  const router = useRouter();
  const emailRef = useRef(null);
  const passwordRef = useRef(null);
  const dispatch = useDispatch();

  const handleSubmit = async (event) => {
    event.preventDefault();
    const email = emailRef.current.value;
    const password = passwordRef.current.value;

    const requestConfig = {
      url: "/api/auth/signin",
      method: "POST",
      headers: { "content-type": "application/json" },
      body: {
        email,
        password,
      },
    };

    const data = await sendRequest(requestConfig);
    if (!isLoading && !error && data) {
      closeModal();
      dispatch(userActions.setUserData({ ...data.userDetails }));
      router.replace("/");
    }
  };

  return (
    <Fragment>
      <p className="modalHeading">Login</p>
      <form className={styles.form} onSubmit={handleSubmit}>
        <InputBox
          id="user_email"
          Icon={MdEmail}
          title="Enter your email"
          type="email"
          inputRef={emailRef}
        />
        <InputBox
          id="user_password"
          Icon={RiLockPasswordFill}
          title="Enter your password"
          type="password"
          inputRef={passwordRef}
        />
        {error && <p className={styles.errText}>{error}</p>}
        {isLoading ? <Spinner /> : <button>Login</button>}
      </form>
      {!isLoading && (
        <div className="extras">
          <p onClick={() => formDispatch({type: ACTIONS.ForgotPassword})}>Forgot Password</p>
        </div>
      )}
    </Fragment>
  );
};

export default LoginForm;



