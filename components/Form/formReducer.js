import LoginForm from "./LoginForm";
import ForgotPassword from "./ForgotPassword";
import ForgotEmail from "./ForgotEmail";
import EnterOtp from "./EnterOtp";
import NewPassword from "./NewPassword";

export const ACTIONS = {
  Login: "login",
  ForgotPassword: "forgot",
  SendOtp: "send-otp",
  SetError: "set-error",
  SetPassword: "set-new-password",
  ForgotEmail: "forgot-email",
};

export const formReducer = (state, action) => {
  switch (action.type) {
    case ACTIONS.Login:
      return { ...state, FORM: LoginForm };
    case ACTIONS.ForgotPassword:
      return { ...state, FORM: ForgotPassword, error: null };
    case ACTIONS.SendOtp:
      return {
        ...state,
        email: action.payload.email,
        FORM: EnterOtp,
        error: null,
      };
    case ACTIONS.SetPassword:
      return {
        ...state,
        FORM: NewPassword,
        otp: action.payload.otp,
        error: null,
      };
    case ACTIONS.SetError:
      return {
        ...state,
        error: action.payload.error,
      };
    case ACTIONS.ForgotEmail:
      return {
        ...state,
        FORM: ForgotEmail,
      };
    default:
      return state;
  }
};
