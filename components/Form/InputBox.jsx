import { Fragment, useState } from "react";
import { AiFillCheckSquare, AiOutlineCheckSquare } from "react-icons/ai";

const InputBox = ({ id, Icon, title, type, inputRef, value = undefined }) => {
  const [showPassword, setShowPassword] = useState(false);

  const togglePass = () => {
    setShowPassword((p) => !p);
  };

  return (
    <Fragment>
      <div>
        <label htmlFor={id}>
          <Icon /> {title}
        </label>
        <input
          type={`${showPassword ? "text" : type}`}
          id={id}
          ref={inputRef}
          required
          value={value}
          onChange={() => {}}
        />
      </div>

      {/* If input box type is Password, show password option will shown */}
      {type === "password" && (
        <div onClick={togglePass}>
          <label htmlFor="showPass" style={{ cursor: "pointer" }}>
            {showPassword ? <AiFillCheckSquare /> : <AiOutlineCheckSquare />}
            show password
          </label>
        </div>
      )}
    </Fragment>
  );
};

export default InputBox;
