import { useReducer } from "react";
import { formReducer } from "./formReducer";
import LoginForm from "./LoginForm";

const FormControl = ({ closeModal }) => {
  const [state, dispatch] = useReducer(formReducer, {
    FORM: LoginForm,
    otp: null,
    error: null,
  });

  return (
    <div>
      <state.FORM
        closeModal={closeModal}
        formDispatch={dispatch}
        state={state}
      />
    </div>
  );
};

export default FormControl;
