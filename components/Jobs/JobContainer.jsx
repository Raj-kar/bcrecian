/* eslint-disable @next/next/no-img-element */
import { Fragment, useState } from "react";
import Sidebar from "../Sidebar/Sidebar";
import styles from "./Job.module.scss";

const stripData = (data) => {
  const len = data.length;
  return len > 23 ? `${data.slice(0, 15)}...${data.slice(len - 3, len)}` : data;
};

const JobContainer = ({ job, handleFilter, handleOngoing }) => {
  const [isShow, setIsShow] = useState(false);
  const [content, setContent] = useState(null);

  const openModal = () => {
    setIsShow(true);
  };

  const closeModal = () => {
    setIsShow(false);
  };

  const handleDetails = (jobBody) => {
    setContent(jobBody);
    openModal();
  };

  return (
    <Fragment>
      <div className={styles.container}>
        <img src={job.img_url} alt="test" />
        <div className={styles.right_container}>
          <div className={styles.inner}>
            <p onClick={() => handleDetails(job.body)}>
              {stripData(job.title)}
            </p>
            <div
              className={styles.status}
              onClick={() => handleOngoing(job.ongoing)}
            >
              {job.ongoing ? (
                <p>Ongoing</p>
              ) : (
                <p className={styles.close}>Closed</p>
              )}
            </div>
          </div>
          <div className={styles.extras}>
            {job.tags.map((tag, index) => (
              <p
                key={index}
                className={styles.type}
                onClick={() => handleFilter(tag)}
              >
                {tag}
              </p>
            ))}
          </div>
          <div className={styles.buttons}>
            <a target="_blank" rel="noreferrer" href={job.apply_link}>
              Apply Here
            </a>
            <p onClick={() => handleDetails(job.body)}>Read more</p>
          </div>
        </div>
      </div>
      {isShow && (
        <Sidebar closeModal={closeModal} isOpen={isShow}>
          <div className={styles.content}>
            <p dangerouslySetInnerHTML={{ __html: content }} />
          </div>
        </Sidebar>
      )}
    </Fragment>
  );
};

export default JobContainer;
