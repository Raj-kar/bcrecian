import { useCallback, useEffect, useState } from "react";
import { EndPoint } from "../../helpers/api/ENDPOINT";
import JobContainer from "./JobContainer";

import useHttp from "../../helpers/hooks/useHttp";
import Spinner from "../Spinner/Spinner";

import styles from "./Job.module.scss";

const Jobs = () => {
  const { isLoading, error, sendRequest: fetchJobs } = useHttp();
  const [jobs, setJobs] = useState(null);
  const [filters, setFilters] = useState([]);

  const fetchData = useCallback(() => {
    const requestConfig = {
      url: `${EndPoint}/jobs/`,
    };
    fetchJobs(requestConfig, setJobs);
  }, [fetchJobs]);

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  const handleFilter = (TagName) => {
    setJobs((prev) => prev.filter((each) => each.tags.includes(TagName)));
    setFilters((prev) => prev.concat(TagName));
  };

  const handleOngoing = (status) => {
    setJobs((prev) => prev.filter((each) => each.ongoing === status));
    setFilters((prev) => prev.concat(status));
  };

  const clearFilter = () => {
    fetchData();
    setFilters([]);
  };

  return (
    <>
      <div className={styles.filterList}>
        {filters.length > 0 &&
          filters.map((each) => (
            <p key={each}>
              {each} <input type="checkbox" checked />{" "}
            </p>
          ))}
      </div>
      <div className={styles.main}>
        {!jobs && !isLoading && error && <p>{error.message}</p>}
        {isLoading && <Spinner />}
        {jobs &&
          !isLoading &&
          jobs.map((job) => (
            <JobContainer
              key={job.id}
              job={job}
              handleFilter={handleFilter}
              handleOngoing={handleOngoing}
            />
          ))}
      </div>
      {filters.length > 0 && (
        <button className="center" onClick={clearFilter}>
          Clear Filter
        </button>
      )}
    </>
  );
};

export default Jobs;
