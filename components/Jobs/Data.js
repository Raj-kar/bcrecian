export const JobData = [
  {
    id: 5,
    title: "Larsen & Toubro Infotech",
    ongoing: true,
    img_url:
      "https://bsmedia.business-standard.com/media-handler.php?mediaPath=http://bsmedia.business-standard.com/_media/bs/img/article/2015-08/11/full/1439286359-1482.jpg&width=1200",
    apply_link: "http://google.com",
    tags: ["On campus", "LTI",],
  },
  {
    id: 4,
    title: "TCS Smart Hiring 2022",
    ongoing: true,
    img_url:
      "https://pbs.twimg.com/profile_images/1375355015594840066/-J9PDpm2_400x400.jpg",
    apply_link: "http://google.com",
    tags: ["off campus", "TCS"],
  },
  {
    id: 2,
    title: "Indus Net Technologies",
    img_url:
      "https://superbcompanies.com/static/images/indus-net-technologies-pvt-ltd.png",
    ongoing: false,
    apply_link: "http://google.com",
    tags: ["On campus", "INT", "BCREC"],
  },
  {
    id: 1,
    title: "Wipro Wilp Program",
    img_url:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a0/Wipro_Primary_Logo_Color_RGB.svg/1200px-Wipro_Primary_Logo_Color_RGB.svg.png",
    ongoing: false,
    apply_link: "http://google.com",
    tags: ["On campus", "Wipro", "BCREC"],
  },
  {
    id: 3,
    title: "SAP LAB",
    img_url:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ8MZ9W2CUCS2MbeOTBYZ6Gx6lvBSM6xJd84Y5u4GIujHiFfUVG4AQRew9kCLFHjrHeUyI&usqp=CAU",
    ongoing: false,
    apply_link: "http://google.com",
    tags: ["On campus", "SAP", "BCREC"],
  },
];
