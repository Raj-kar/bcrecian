/* eslint-disable @next/next/no-img-element */
import { useState } from "react";
import { IoIosArrowDown, IoIosArrowUp } from "react-icons/io";

import styles from "./NotifyBar.module.scss";

const FallBackImg =
  "https://i.ibb.co/TLkQPGR/cute-astronaut-reading-book-cartoon-vector-icon-illustration-science-education-icon-concept-isolated.jpg";

const stripData = (data) => {
  const len = data.length;
  return len > 40 ? `${data.slice(0, 40)}...` : data;
};

const NotifyContainer = ({ notify, isNew }) => {
  const [showDetails, setShowDetails] = useState(false);

  const handleShowDetails = () => {
    setShowDetails((prev) => !prev);
  };

  return (
    <div className={`${styles.container} ${isNew ? styles.newNoti : ""}`}>
      <img src={notify.imgUrl || FallBackImg} alt={notify.title} />
      <aside>
        {!notify?.title ? (
          <p>You are all caught up for now.</p>
        ) : (
          <>
            <p>{notify.title}</p>
            <p>{showDetails ? notify.body : stripData(notify.body)}</p>
            <div className={styles.extras}>
              <p>{notify.date}</p>
              <span onClick={handleShowDetails}>
                {showDetails ? (
                  <IoIosArrowUp className="icon" />
                ) : (
                  <IoIosArrowDown className="icon" />
                )}
              </span>
            </div>
          </>
        )}
      </aside>
    </div>
  );
};

export default NotifyContainer;
