import ReactDOM from "react-dom";
import { useCallback, useEffect, useRef, useState } from "react";
import { useSelector } from "react-redux";
import NotifyContainer from "./NotifyContainer";
import NotifyLoader from "./NotifyLoader";
import useHttp from "../../helpers/hooks/useHttp";

import styles from "./NotifyBar.module.scss";
import { EndPoint } from "../../helpers/api/ENDPOINT";

const NotifyBar = ({ closeModal, isOpen, count, getNotiCount }) => {
  const overlayRef = useRef();
  const [isBrowser, setIsBrowser] = useState(false);
  const { isLoading, error, sendRequest: fetchNoti } = useHttp();
  const [noti, setNoti] = useState(null);
  const { course, section, semester, roll, email } = useSelector(
    (state) => state.user
  );

  useEffect(() => {
    setIsBrowser(true);

    return () => {
      getNotiCount();
    };
  }, [getNotiCount]);

  const handleClick = (event) => {
    if (overlayRef.current === event.target) closeModal();
  };

  // Close the modal pressing Escape key
  const keyDownCheck = useCallback(
    (e) => {
      if (e.key === "Escape" && isOpen) closeModal();
    },
    [closeModal, isOpen]
  );

  // Binding Keys
  useEffect(() => {
    document.addEventListener("keydown", keyDownCheck);

    return () => {
      document.removeEventListener("keydown", keyDownCheck);
    };
  }, [keyDownCheck]);

  //   Fetch Notification
  useEffect(() => {
    const requestConfig = {
      url: `${EndPoint}/get-notifications/`,
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: { stream: course, semester, section, roll, email },
    };
    fetchNoti(requestConfig, setNoti);
  }, [course, roll, section, semester, fetchNoti, email, count]);

  const BarContent = (
    <div className={styles.overlay} ref={overlayRef} onClick={handleClick}>
      <div className={styles.notify}>
        <ul>
          {isLoading && <NotifyLoader />}
          {!isLoading && count > 0 && (
            <>
              <li className={styles.indent}>Recents</li>
              {!isLoading &&
                noti?.payload.length > 0 &&
                noti.payload
                  .slice(0, count)
                  .map((notify, index) => (
                    <NotifyContainer
                      key={index}
                      notify={notify}
                      isNew={index < count}
                    />
                  ))}
            </>
          )}
          <li className={styles.indent}>Earlier</li>
          {!isLoading && noti?.payload.length > count ? (
            noti.payload
              .slice(count)
              .map((notify, index) => (
                <NotifyContainer key={index} notify={notify} />
              ))
          ) : (
            <NotifyContainer
              notify={
                error
                  ? {
                      title: "503 - Lost on Space",
                      body: "Sorry, We are sucked up! Try after sometime...",
                    }
                  : {}
              }
            />
          )}
        </ul>
      </div>
    </div>
  );

  if (isBrowser) {
    return ReactDOM.createPortal(
      BarContent,
      document.getElementById("modal-root")
    );
  } else {
    return null;
  }
};

export default NotifyBar;
