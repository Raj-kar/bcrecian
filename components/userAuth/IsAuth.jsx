import { useSelector } from "react-redux";
import VerifyUser from "../Form/VerifyUser";
import ModalConfig from "../Modal/ModalConfig";
import styles from "./UserAuth.module.scss";

const IsAuth = () => {
  const { name } = useSelector((state) => state.user);

  return (
    <div className={styles.container}>
      <p>Welcome {name}</p>
      <p className={styles.msg}>
        Now, you need verify your account for continue !
      </p>
      <ModalConfig buttonText="Verify" Component={VerifyUser} />
    </div>
  );
};

export default IsAuth;
