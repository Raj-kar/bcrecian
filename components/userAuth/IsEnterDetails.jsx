import { useState } from "react";
import UserDetails from "../Form/UserDetails";
import Modal from "../Modal/Modal";
import styles from "./UserAuth.module.scss";

const IsEnterDetails = () => {
  const [isShow, setIsShow] = useState(false);

  const openModal = () => {
    setIsShow(true);
  };

  const closeModal = () => {
    setIsShow(false);
  };

  return (
    <div className={styles.container}>
      <p>🎉 You are all set to Go 🎉</p>
      <p> Add your academic details to getting started.</p>
      <button onClick={openModal}>ADD</button>
      {isShow && (
        <Modal closeModal={closeModal} isOpen={isShow}>
          <UserDetails />
        </Modal>
      )}
    </div>
  );
};

export default IsEnterDetails;
