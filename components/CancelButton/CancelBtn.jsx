import { IoCloseSharp } from "react-icons/io5";
import styles from "./CancelBtn.module.scss";

const CancelBtn = ({ closeModal }) => {
  return (
    <div className={styles.cancel}>
      <IoCloseSharp onClick={closeModal} />
    </div>
  );
};

export default CancelBtn;
