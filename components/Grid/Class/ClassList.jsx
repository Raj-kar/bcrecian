import { useState } from "react";
import { getDay } from "../../../helpers/functions/getDay";
import isOnGoing from "../../../helpers/functions/isOnGoing";
import Modal from "../../Modal/Modal";
import classes from "./Class.module.scss";
import PaperDetails from "./PaperDetails";

export const trimTime = (time) => time.slice(0, 5);

const ClassList = ({ timetable, isToday }) => {
  const [isShow, setIsShow] = useState(false);
  const [data, setData] = useState("loading.....");

  const closeModal = () => {
    setIsShow(false);
  };

  const handleClick = (data) => {
    setData(data);
    setIsShow(true);
  };

  return (
    <div>
      {isShow && (
        <Modal closeModal={closeModal} isOpen={isShow}>
          <PaperDetails data={data} />
        </Modal>
      )}

      <ul className={classes.list}>
        {timetable &&
          timetable.map((table) => (
            <li
              key={table.paper.code}
              className={`${classes.item} ${
                isOnGoing(table.start_time, table.end_time) &&
                isToday == getDay()
                  ? classes.onGoing
                  : ""
              }`}
            >
              <span onClick={() => handleClick(table)}>
                {table.paper.code.slice(0, 8)}
              </span>
              <span>
                {trimTime(table.start_time)}-{trimTime(table.end_time)}
              </span>
              {table.is_cancel && <span>&#10060;</span>}
              {table.teacher.class_link && !table.is_cancel && (
                <a
                  target="_blank"
                  rel="noreferrer"
                  href={table.teacher.class_link}
                >
                  JOIN
                </a>
              )}
              {!table.is_cancel && !table.teacher.class_link && (
                <span>_____</span>
              )}
            </li>
          ))}
      </ul>
      {timetable && (
        <p className="hint">
          Hint: Click on the paper code, for showing more details.
        </p>
      )}
    </div>
  );
};

export default ClassList;
