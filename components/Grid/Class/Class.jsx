import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import Container from "../../Container/Container";
import classes from "./Class.module.scss";
import Tippy from "@tippyjs/react";
import RefreshButton from "./RefreshButton";

import { formatAMPM } from "../../../helpers/functions/getCurTime";
import ClassList from "./ClassList";
import Spinner from "../../Spinner/Spinner";
import useHttp from "../../../helpers/hooks/useHttp";
import { EndPoint } from "../../../helpers/api/ENDPOINT";

import { IoIosArrowBack, IoIosArrowForward } from "react-icons/io";
import { dateToLocalDate, getDay } from "../../../helpers/functions/getDay";

const Class = () => {
  let { isLoading, error, sendRequest } = useHttp();
  const [routine, setRoutine] = useState(null);
  const [current, setCurrent] = useState({
    time: formatAMPM(),
    date: null,
    day: getDay(),
  });
  const { course, semester, section } = useSelector((state) => state.user);

  useEffect(() => {
    const requestConfig = {
      url: `${EndPoint}/timetable/?course=${course}&semester=${semester}&section=${section}&day=${current.day}`,
    };
    sendRequest(requestConfig, setRoutine);

    // const reCall = setInterval(() => {
    //   console.log("called timetable API");
    // }, 2000);

    return () => {
      console.log("Calling API stopped !!!");
      // clearInterval(reCall);
    };
  }, [sendRequest, course, semester, section, current]);

  const setCurrentTime = () => {
    setCurrent({ time: formatAMPM(), date: null, day: getDay() });
  };

  const handleRoutine = (isNext) => {
    let date;
    if (!current.date) date = new Date().getDate();
    else date = current.date;

    if (isNext)
      setCurrent({
        ...current,
        day: current.day === 7 ? 1 : current.day + 1,
        date: date + 1,
      });
    else
      setCurrent({
        ...current,
        day: current.day === 1 ? 7 : current.day - 1,
        date: date - 1,
      });
  };

  return (
    <Container>
      <div className={classes.status}>
        {current.date ? (
          <p>{dateToLocalDate(current.date)}</p>
        ) : (
          <p>Last Update on : {current.time}</p>
        )}
        <Tippy content="click to refersh" placement={"top-start"}>
          <RefreshButton isLoaing={isLoading} handleFetch={setCurrentTime} />
        </Tippy>
      </div>
      <div className={classes.inner}>
        <div className={classes.msg}>
          <span>{!isLoading && !error && routine?.message}</span>
        </div>
        <div className={classes.routine}>
          <IoIosArrowBack
            className="icon"
            onClick={() => handleRoutine(false)}
          />
          {isLoading ? (
            <div className="centerText">
              <Spinner />
            </div>
          ) : (
            <ClassList timetable={routine?.payload} isToday={current.day} />
          )}
          <IoIosArrowForward
            className="icon"
            onClick={() => handleRoutine(true)}
          />
        </div>
        {!isLoading && error && <p>{error.message}</p>}
      </div>
    </Container>
  );
};

export default Class;
