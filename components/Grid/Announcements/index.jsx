import { useEffect, useState } from "react";
import { EndPoint } from "../../../helpers/api/ENDPOINT";
import useHttp from "../../../helpers/hooks/useHttp";
import Container from "../../Container/Container";
import Modal from "../../Modal/Modal";
import Spinner from "../../Spinner/Spinner";
import classes from "./Announcements.module.scss";
import Details from "./Details";

const Announcements = () => {
  const { isLoading, error, sendRequest: fetchAnnounce } = useHttp();
  const [announce, setAnnounce] = useState(null);
  const [isShow, setIsShow] = useState(false);
  const [data, setData] = useState("loading.....");

  const closeModal = () => {
    setIsShow(false);
  };

  const handleClick = (data) => {
    setData(data);
    setIsShow(true);
  };

  useEffect(() => {
    const requestConfig = {
      url: `${EndPoint}/announcements/`,
    };
    fetchAnnounce(requestConfig, setAnnounce);
  }, [fetchAnnounce]);

  return (
    <Container>
      {isShow && (
        <Modal closeModal={closeModal} isOpen={isShow}>
          <Details data={data} />
        </Modal>
      )}

      {isLoading && !announce && <Spinner />}
      {!isLoading && error && <p>{error.message}</p>}
      <ol className={classes.list} type="a">
        {!isLoading &&
          !error &&
          announce &&
          announce.payload.map((ann, index) => (
            <li
              className={classes.items}
              key={index}
              onClick={() => handleClick(ann)}
            >
              {ann.title}
            </li>
          ))}
      </ol>
      <p className="hint">Hint: Click on the title to read more.</p>
    </Container>
  );
};

export default Announcements;
