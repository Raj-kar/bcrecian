import { useRouter } from "next/router";
import { FiLogOut } from "react-icons/fi";
import { useDispatch } from "react-redux";
import { userActions } from "../../store/user";

import styles from "./UserProfile.module.scss";
// ==============================================
import { initializeApp } from "firebase/app";
import { getMessaging, getToken, deleteToken } from "firebase/messaging";

const firebaseConfig = {
  apiKey: "AIzaSyDC94VsmZh0YWkcrdeR_s8WZtg712DP5Bw",
  authDomain: "django-notification-b0084.firebaseapp.com",
  projectId: "django-notification-b0084",
  storageBucket: "django-notification-b0084.appspot.com",
  messagingSenderId: "608576668692",
  appId: "1:608576668692:web:f6a710d7d3346ec8e1d301",
  measurementId: "G-Y98CRFQ6Z6"
};
const LogoutButton = () => {
  const dispatch = useDispatch();
  const router = useRouter();

  const handleLogout = async () => {
    // ===========================
    const app = initializeApp(firebaseConfig);
    const messaging = getMessaging(app)

    getToken(messaging).then((currentToken) => {
      if (currentToken) {
        console.log(currentToken)
        fetch('/api/auth/removenotificationtoken?token=' + currentToken).then((res => {
          console.log(res.json())
        })).catch((err) => {
          console.log(err)
        })
      } else {
        console.log('No registration token available. Request permission to generate one.');

      }
    }).catch((err) => {
      console.log('An error occurred while retrieving token. ', err);
    });

    deleteToken(messaging).then((message) => {
      console.log('message ' + message)
    }).catch((err) => {
      console.log('error ' + err)
    })
    // ====================================
    const response = await fetch("/api/auth/logout", {
      method: "POST",
    });

    if (response.ok) {
      router.push({
        pathname: "/",
        query: { status: "logout" },
      });
      dispatch(userActions.logOut());
    } else {
      console.log("something went wrong, while LOGOUT !");
    }
  };

  return (
    <button className={styles.button} onClick={handleLogout}>
      Logout
      <FiLogOut className="icon" />
    </button>
  );
};

export default LogoutButton;
