import styles from "./Log.module.scss";
import { GrFormAdd } from "react-icons/gr";
import { useState } from "react";
import Sidebar from "../../Sidebar/Sidebar";
import AddData from "./AddData";
import useHttp from "../../../helpers/hooks/useHttp";
import Spinner from "../../Spinner/Spinner";
import { EndPoint } from "../../../helpers/api/ENDPOINT";
import { useSelector } from "react-redux";
import LogTable from "./LogTable";

const StdLogbook = () => {
  const [showForm, setShowForm] = useState(false);
  const { isLoading, error, sendRequest: fetchData } = useHttp();
  const [data, setData] = useState(null);
  const { course, semester, section } = useSelector(
    (state) => state.user
  );

  const handleClick = () => {
    setShowForm((prev) => !prev);
    const requestConfig = {
      url: `${EndPoint}/classdetails/?course=${course}&semester=${semester}&section=${section}`,
    };
    fetchData(requestConfig, setData);
  };

  return (
    <div className={styles.container}>
      <button onClick={handleClick}>
        <GrFormAdd className="icon" />
      </button>
      <LogTable />
      {showForm && (
        <Sidebar closeModal={handleClick} isOpen={showForm}>
          {isLoading && <Spinner />}
          {!isLoading && !error && !data?.message && data && (
            <AddData data={data.payload} />
          )}
          {!isLoading && !error && data?.message && (
            <p className={styles.info}>{data.message}</p>
          )}
          {!isLoading && error && (
            <p className={styles.info}>{error.message}</p>
          )}
        </Sidebar>
      )}
    </div>
  );
};

export default StdLogbook;
