import { AiOutlinePaperClip } from "react-icons/ai";
import { MdAccessTime, MdDateRange } from "react-icons/md";
import { ImBook } from "react-icons/im";
import { FaChalkboardTeacher } from "react-icons/fa";
import { BiBookReader } from "react-icons/bi";
import InputBox from "../../Form/InputBox";
import styles from "./Log.module.scss";

const currentDate = () => {
  const date = new Date();
  let c_date = date.getDate();
  if (c_date < 10) c_date = "0" + c_date;
  const current_date = `${date.getFullYear()}-${date.getMonth() + 1}-${c_date}`;
  return current_date;
};

const AddData = ({ data }) => {
  return (
    <div className={styles.sidebar}>
      <form className={styles.form}>
        <InputBox
          id="date"
          Icon={MdDateRange}
          title="Today's Date"
          type="date"
          value={currentDate()}
        />
        <InputBox
          id="start_time"
          Icon={MdAccessTime}
          title="Class start time"
          type="time"
          value={data.start_time}
        />
        <InputBox
          id="end_time"
          Icon={MdAccessTime}
          title="Class ending time"
          type="time"
          value={data.end_time}
        />
        <InputBox
          id="paper_code"
          Icon={AiOutlinePaperClip}
          title="Paper code"
          value={data.paper.code}
        />
        <InputBox
          id="paper_name"
          Icon={ImBook}
          title="Paper name"
          value={data.paper.name}
        />
        <InputBox
          id="teacher_name"
          Icon={FaChalkboardTeacher}
          title="Teacher name"
          value={`${data.teacher.name} (${data.teacher.slug})`}
        />
        <div className={styles.topics}>
          <label htmlFor="topics">
            <BiBookReader />
            Topics
          </label>
          <textarea name="Topics" id="topics" rows="2" cols="30" />
        </div>
        <button>Add Record</button>
      </form>
    </div>
  );
};

export default AddData;
