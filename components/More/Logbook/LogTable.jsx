import { useEffect, useState } from "react";
import { AiOutlineDelete } from "react-icons/ai";
import { FaEdit } from "react-icons/fa";
import { useSelector } from "react-redux";
import { EndPoint } from "../../../helpers/api/ENDPOINT";
import useHttp from "../../../helpers/hooks/useHttp";
import Sidebar from "../../Sidebar/Sidebar";
import Spinner from "../../Spinner/Spinner";

import styles from "./Log.module.scss";

const LogTable = () => {
  const [logData, setLogData] = useState(null);
  const [isEdit, setIsEdit] = useState(false);
  const { isLoading, error, sendRequest: fetchData } = useHttp();
  const { course, semester, section } = useSelector((state) => state.user);

  //   Fetch Logbook details
  useEffect(() => {
    console.log("Fetch Log BOOK RECORDS");
    const requestConfig = {
      url: `${EndPoint}/logs/?course=${course}&semester=${semester}&section=${section}`,
    };
    fetchData(requestConfig, setLogData);
  }, [course, semester, section, fetchData]);

  const updateSignature = async (logId) => {
    //   Change in State for giving a user feedback of real time changes
    const newLog = logData.payload.map((log) =>
      log.id === logId ? { ...log, signature: !log.signature } : log
    );
    setLogData({ ...logData, payload: newLog });

    // Make Changes in the actual DB
    const response = await fetch(`${EndPoint}/logs/`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ id: logId }),
    });
    const data = await response.json();
    console.log(data);
  };

  const deleteLog = async (logId) => {
    //   Change in State for giving a user feedback of real time changes
    const newLog = logData.payload.filter((log) => log.id !== logId);
    setLogData({ ...logData, payload: newLog });

    // Make Changes in the actual DB
    const response = await fetch(`${EndPoint}/logs/`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ id: logId }),
    });
    const data = await response.json();
    console.log(data);
  };

  const handleSidebar = () => {
    setIsEdit((prev) => !prev);
  };

  return (
    <>
      {isEdit && (
        <Sidebar closeModal={handleSidebar} isOpen={isEdit}>
          HERE FORM WILLL BE SHOWN !
        </Sidebar>
      )}
      {isLoading && <Spinner />}
      <table>
        <thead>
          <tr>
            <td>Date</td>
            <td>Duration</td>
            <td>Paper Code</td>
            <td>Teacher Name</td>
            <td>Topics</td>
            <td>Signature</td>
            <td>Modify</td>
          </tr>
        </thead>
        <tbody>
          {!isLoading &&
            !error &&
            logData &&
            logData.payload.map((log) => (
              <tr key={log.id}>
                <td>{log.date}</td>
                <td>
                  {log.start_time.slice(0, 5)} - {log.end_time.slice(0, 5)}
                </td>
                <td>{log.paper_code}</td>
                <td>{log.teacher_name.name}</td>
                <td>{log.topics}</td>
                <td>
                  <input
                    type="checkbox"
                    className={styles.checkbox}
                    checked={log.signature}
                    onChange={() => {
                      updateSignature(log.id);
                    }}
                  />
                </td>
                <td className={styles.more}>
                  <FaEdit
                    onClick={() => handleSidebar(log.id)}
                    className="icon"
                  />
                  <AiOutlineDelete
                    onClick={() => {
                      deleteLog(log.id);
                    }}
                    className="icon"
                  />
                </td>
              </tr>
            ))}
        </tbody>
      </table>
    </>
  );
};

export default LogTable;
