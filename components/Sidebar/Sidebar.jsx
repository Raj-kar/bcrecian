import { useCallback, useEffect, useRef, useState } from "react";
import ReactDOM from "react-dom";
import CancelBtn from "../CancelButton/CancelBtn";
import styles from "./Sidebar.module.scss";

const Sidebar = ({ closeModal, isOpen, children }) => {
  const overlayRef = useRef();
  const [isBrowser, setIsBrowser] = useState(false);

  useEffect(() => {
    setIsBrowser(true);
  }, []);

  const handleClick = (event) => {
    if (overlayRef.current === event.target) closeModal();
  };

  // Close the modal pressing Escape key
  const keyDownCheck = useCallback(
    (e) => {
      if (e.key === "Escape" && isOpen) closeModal();
    },
    [closeModal, isOpen]
  );

  useEffect(() => {
    document.addEventListener("keydown", keyDownCheck);

    return () => {
      document.removeEventListener("keydown", keyDownCheck);
    };
  }, [keyDownCheck]);

  const SideBarContent = (
    <div className={styles.overlay} ref={overlayRef} onClick={handleClick}>
      <div className={styles.modal}>
        <CancelBtn closeModal={closeModal} />
        {children}
      </div>
    </div>
  );

  if (isBrowser) {
    return ReactDOM.createPortal(
      SideBarContent,
      document.getElementById("modal-root")
    );
  } else {
    return null;
  }
};

export default Sidebar;
