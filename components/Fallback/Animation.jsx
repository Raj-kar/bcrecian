import styles from "./Animation.module.scss";

const Animation = () => {
  return (
    <div className={styles.middle}>
      <div className={styles.bar1}></div>
      <div className={styles.bar2}></div>
      <div className={styles.bar3}></div>
      <div className={styles.bar4}></div>
      <div className={styles.bar5}></div>
      <div className={styles.bar6}></div>
      <div className={styles.bar7}></div>
      <div className={styles.bar8}></div>
    </div>
  );
};

export default Animation;
