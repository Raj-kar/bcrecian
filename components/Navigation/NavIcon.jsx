import { useRouter } from "next/router";
import { Fragment } from "react";
import { useSelector } from "react-redux";
import ModalConfig from "../Modal/ModalConfig";
import UserProfile from "../UserProfile/UserProfile";
import Message from "./Message";

const MSG = () => <p>You have Successfully Logout.</p>;

const NavIcon = ({ Icon }) => {
  const { isAddedDetails } = useSelector((state) => state.user);
  const router = useRouter();

  if (router.query.status === "logout") {
    return <ModalConfig Component={MSG} buttonText={<Icon />} />;
  }

  return (
    <Fragment>
      <ModalConfig
        Component={isAddedDetails ? UserProfile : Message}
        buttonText={<Icon />}
      />
    </Fragment>
  );
};

export default NavIcon;
