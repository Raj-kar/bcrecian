import { Fragment } from "react";
import styles from "./Section.module.scss";

const Section = ({ title, Component, Icon, FilterCompo = null }) => {
  return (
    <Fragment>
      <div className={styles.heading}>
        <Icon className={styles.icon} />
        <h2>{title}</h2>
      </div>
      <Component />
    </Fragment>
  );
};

export default Section;
