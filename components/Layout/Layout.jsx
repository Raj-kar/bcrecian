import { Fragment, useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { userActions } from "../../store/user";
import { useRouter } from "next/router";
import Meta from "../../meta/Meta";
import Spinner from "../Spinner/Spinner";
import Pages from "./Pages";

import styles from "./Layout.module.scss";
import Fallback from "../Fallback/Fallback";

const verifyUserStatus = async () => {
  // console.log("CALLED");
  try {
    const response = await fetch("/api/auth/isauthorized/");
    if (!response.ok) undefined;
    const data = await response.json();
    return data.userDetails;
  } catch (err) {
    return false;
  }
};

const Layout = ({ children }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [hasError, setError] = useState(false);

  const router = useRouter();
  const dispatch = useDispatch();

  useEffect(() => {
    const fetchUserData = async () => {
      setIsLoading(true);
      const data = await verifyUserStatus();
      // console.log("data", data);
      setIsLoading(false);

      if (data === false) {
        setError(true);
      }

      if (!data) {
        return;
      }

      if (data.isAddedDetails) {
        if (router.pathname === "/") {
          router.replace("/");
        }
        dispatch(userActions.setUserData({ ...data }));
      }

      if (data && !data.isVerified) {
        const { name: userName, email: userEmail, roll: userRoll } = data;
        router.replace("/");
        dispatch(
          userActions.loginUser({
            userName,
            userEmail,
            userRoll,
          })
        );
      }

      if (data && data.isVerified && !data.isAddedDetails) {
        router.replace("/");
        dispatch(userActions.setUserData({ ...data }));
      }
    };

    fetchUserData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch]);

  if (hasError) {
    return <Fallback />;
  }

  if (isLoading)
    return (
      <div className={styles.center}>
        <Spinner />
      </div>
    );

  return (
    <Fragment>
      <Meta />
      <Pages>{children}</Pages>
    </Fragment>
  );
};

export default Layout;
