import React, { Fragment } from "react";
import { useSelector } from "react-redux";
import useMediaQuery from "../../helpers/hooks/useMediaQuery";

import Nav from "../Navigation/Nav";
import IsAuth from "../userAuth/IsAuth";
import IsEnterDetails from "../userAuth/IsEnterDetails";
import MobileNavigation from "../Mobile/Navigation/Navigation";

import styles from "./Layout.module.scss";
import Home from "../Home/Home";

const Pages = ({ children }) => {
  const isMobile = useMediaQuery("(max-width: 768px)");
  const { isAuth, isVerified, isAddedDetails } = useSelector(
    (state) => state.user
  );

  const MainContent = () => {
    return <Fragment>{children}</Fragment>;
  };

  return (
    <div className={styles.container}>
      <Nav />
      {!isAuth && <Home />}
      {isAuth && !isVerified && <IsAuth />}
      {isAuth && isVerified && !isAddedDetails && <IsEnterDetails />}
      {isAuth && isVerified && isAddedDetails && <MainContent />}
      {isMobile && <MobileNavigation />}
    </div>
  );
};

export default Pages;
