import Section from "../../components/Section/Section";
import { ImBullhorn } from "react-icons/im";
import Jobs from "../../components/Jobs/Jobs";

const Index = () => {
  return (
    <div className="main_container">
      <Section
        title="Jobs Portal"
        Icon={ImBullhorn}
        Component={Jobs}
      />
    </div>
  );
};

export default Index;
