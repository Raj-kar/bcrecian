import Section from "../../components/Section/Section";
import { AiFillSetting } from "react-icons/ai";
import MoreContainer from "../../components/More/MoreContainer";

const index = () => {
  return (
    <div className="main_container">
      <Section title="More" Component={MoreContainer} Icon={AiFillSetting} />
    </div>
  );
};

export default index;
