import { FaSwatchbook } from "react-icons/fa";
import StdLogbook from "../../components/More/Logbook/StdLogbook";
import Section from "../../components/Section/Section";

const logbook = () => {
  return (
    <div className="main_container">
      <Section
        title="Student Logbook"
        Component={StdLogbook}
        Icon={FaSwatchbook}
      />
    </div>
  );
};

export default logbook;
