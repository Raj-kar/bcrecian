require("../DB/conn");
const User = require("../models/userSchema.js");
const OTP = require("../models/otpSchema");
const ObjectId = require("mongoose").Types.ObjectId;
const sendMail = require("../helpers/sendMail");
const bcrypt = require("bcrypt");
const env = require('../config')

export default async function handler(req, res) {
  if (req.method === "POST") {
    const { email } = req.body;
    const userExist = await User.findOne({ email: email });
    if (userExist) {
      if (!userExist.isVerified) {
        return res.status(422).json({ error: "Your account is not verified!" });
      }
      try {
        const otpExist = await OTP.findOne({
          _userId: new ObjectId(userExist._id),
        });
        if (otpExist) {
          otpExist.remove({});
        }
        let otpCode = Math.floor(Math.random() * (9999 - 1000) + 1000);

        const emailBody = {
          head: "Change Your password now! If this was a mistake you can leave.",
          body: `Thank you for using College Space .<br>Please, consider this as an confirmation email. verify the otp for forgetting password .<br> OTP ${otpCode} . <br> If you have any questions contact us with this email address `,
        };
        let emailLink = '#'
        try {
          let otp = new OTP({ _userId: userExist._id, otp: otpCode });
          await otp.save();
          await sendMail(emailLink, email, userExist.name, emailBody, "none");
          // console.log(otp); // TODO: REMOVE IT
          res.status(200).json({ message: "OTP send to the registered email" });
        } catch (err) {
          console.log(err);
          res.status(500).json({ error: "Internal Server error" });
        }
      } catch (err) {
        console.log(err);
        res.status(500).json({ error: "Internal Server error" });
      }
    } else {
      return res.status(422).json({ error: "No User is Registered with this email." });
    }
  } else {
    return res.status(422).json({ error: "Method not allowed" });
  }
}
