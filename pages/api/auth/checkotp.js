require("../DB/conn");
const User = require("../models/userSchema");
const OTP = require("../models/otpSchema");
const env = require("../config");

export default async function handler(req, res) {
  if (req.method == "POST") {
    const { email, otp } = req.body;
    // console.log(email)
    if (!email || !otp) {
      return res.status(422).json({ error: "Fields cannot be null" });
    }
    try {
      const userExist = await User.findOne({ email: email });
      if (userExist) {
        // console.log(userExist)
        const OTPExist = await OTP.findOne({ _userId: userExist._id });
        if (OTPExist) {
          // console.log(OTPExist)
          if (OTPExist.otp == otp) {
            return res.status(200).json({ message: "Otp matched" });
          } else {
            return res.status(422).json({ error: "Otp Mismatched." });
          }
        } else {
          return res.status(422).json({ error: "Otp expired" });
        }
      } else {
        return res.status(422).json({ error: "Email is not registered" });
      }
    } catch (e) {
      console.log(e);
      return res.status(500).json({ error: "Internal server error" });
    }
  } else {
    return res.status(405).json({ error: "Method not Allowed" });
  }
}
