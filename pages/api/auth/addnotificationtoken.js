require("../DB/conn");
const jwt = require("jsonwebtoken");
const Cookies = require("cookies");
const User = require("../models/userSchema");
const env = require("../config");

export default async function handler(req, res) {
    try {
        let notification_token = req.query
        notification_token = notification_token.token
        const cookies = new Cookies(req, res);
        // Get a cookie
        const token = cookies.get("jwt");
        if (token) {
            const verifyUser = jwt.verify(token, env.mpJWTSecretKey);
            console.log(verifyUser);
            const userExist = await User.findOne({ _id: verifyUser._id });
            if (userExist) {
                // console.log((userExist.notification_tokens))
                let tokens = userExist.notification_tokens
                let isTokenExist = false
                tokens.every(function (ele, index) {
                    if (ele.token == notification_token) {
                        isTokenExist = true
                        return false
                    }
                    else return true
                });
                if (!isTokenExist) {
                    try {
                        userExist.notification_tokens = userExist.notification_tokens.concat({ token: notification_token });
                        await userExist.save();
                        res.status(200).json({ message: "Notification token added successfully" });
                    }
                    catch (e) {
                        res.status(500).json({ error: "Some error occurs" })
                    }
                }
                else {
                    res.status(200).json({ message: "Token already exist" });

                }
            }
            else {
                res.status(200).json({ error: "User not exist" });
            }
        } else {
            res.status(200).json({ userDetails: undefined });
        }
    } catch (err) {
        console.log(err);
        res.status(422).json({ error: "Invalid cookies" });
    }
}
