require("../DB/conn");
const User = require("../models/userSchema");
const env = require("../config");

export default async function handler(req, res) {
    if (req.method == 'POST') {
        try {
            let filters = (req.body)
            filters = (JSON.parse(filters))
            console.log(filters[0].course)
            console.log(filters[1].semester)
            console.log(filters[2].section)
            console.log(filters[3].roll)
            let query = {}
            if (filters[0].course != "None") {
                query.course = filters[0].course
            }
            if (filters[1].semester) {
                query.semester = filters[1].semester
            }
            if (filters[2].section) {
                query.section = filters[2].section
            }
            if (filters[3].roll) {
                let roll = (filters[3].roll).split(",")
                console.log(roll)
                query.roll = { $in: roll }

            }
            console.log(query)
            let response = await User.find(query)
            // console.log(response)
            if (response == 0) {
                return res.status(500).json({ "error": "no user exist" })
            }
            let name_arr = []
            response.forEach(e => {
                try {
                    e.notification_tokens.forEach(token => {
                        name_arr.push(token.token)
                    })
                } catch (err) { }
            })
            if (name_arr == 0) {
                return res.status(500).json({ "error": "no token exist" })
            }
            console.log(name_arr)
            return res.status(200).json({ "data": name_arr })
        } catch (e) {
            console.log(e)
            return res.status(500).json({ "error": "Internal server error" })
        }
    } else {
        return res.status(403).json({ "error": "Method not allowed!" })
    }
}