require("../DB/conn");
const User = require("../models/userSchema");
const env = require("../config");

export default async function handler(req, res) {
  if (req.method === "POST") {
    const { roll } = req.body;
    if (!roll) {
      return res.status(422).json({ error: "fields cannot be null" });
    }
    var numRegex = /^[0-9]+$/;
    if (!String(roll).match(numRegex)) {
      return res.status(422).json({ error: "roll number should be a number" });
    }
    try {
      let userExist = await User.findOne({ roll: roll });
      if (!userExist) {
        return res.status(422).json({
          error: "No Account found, Registered with this roll number.",
        });
      }
      return res.status(200).json({ message: userExist.email });
    } catch (e) {
      console.log(e);
      return res.status(500).json({ error: "Internal server error" });
    }
  } else {
    return res.status(405).json({ error: "Method not allowed" });
  }
}
