const jwt = require("jsonwebtoken");
var Cookies = require('cookies')
export default async function handler(req, res) {
    if (req.method === 'POST') {
        try {
            const cookies = new Cookies(req, res);
            let token = cookies.get("jwt");
            console.log(token);
            // Set a cookie
            cookies.set("jwt", '', {
                expires: new Date(Date.now()),
                httpOnly: true,
            });
            res.status(200).json({ message: "Logout!" });
        } catch (err) {
            console.log(err)
            res.status(422).json({ error: "please login first" });
        }
    }
    else {
        return res.status(405).json({ error: "Method not allowed" })
    }
}
