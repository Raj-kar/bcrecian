require("../DB/conn");
const User = require("../models/userSchema");
const env = require("../config");
import Moment from "moment";
import NextCors from 'nextjs-cors';
export default async function handler(req, res) {
    await NextCors(req, res, {
        // Options
        methods: ['GET', 'HEAD', 'PUT', 'PATCH', 'POST', 'DELETE'],
        origin: '*',
        optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
    });

    if (req.method == 'GET') {
        const email = req.query.email
        const changeTime = req.query.changetime
        try {
            let userExist = await User.findOne({ email: email })
            if (!userExist) {
                return res.status(403).json({ "error": "User not exist" })
            }
            console.log(userExist)
            let previous_time = userExist.notification_check_time
            if (changeTime == "true") {
                userExist.notification_check_time = Date.now()
                await userExist.save()
            }
            let d = previous_time
            console.log(d)
            try {
                if (d != undefined) {
                    previous_time = Moment(d).format("HH:mm:ss D MMM,YYYY")
                    // previous_time = d;
                }
                else {
                    previous_time = {}
                }
                //previous_time = `${d.getHours()}-${d.getMinutes()}-${d.getSeconds()} - ${d.getDate()}/${d.getMonth("%A") + 1}/${d.getFullYear()}` //(d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear() + ' ' + (d.getHours() > 12 ? d.getHours() - 12 : d.getHours()) + ':' + d.getMinutes() + ' ' + (d.getHours() >= 12 ? "PM" : "AM");
            }
            catch (e) {
                console.log(e)
            }
            return res.status(200).json({ "previous_time": previous_time })

        }
        catch (e) {
            console.log(e)
            return res.status(500).json({ "error": "Internal server error" })
        }
    } else {
        return res.status(403).json({ "error": "Method not allowed!" })
    }
}