require("../DB/conn");
const User = require("../models/userSchema.js");
const OTP = require("../models/otpSchema");
const ObjectId = require("mongoose").Types.ObjectId;
const SendMail = require("../helpers/sendMail");
const validation = require("../validation/validator");

export default async function handler(req, res) {
  if (req.method === "POST") {
    const { otp, email, newPassword } = req.body;
    if (!otp || !email || !newPassword) {
      return res.status(422).json({ error: "fields cannot be null" })
    }
    if (!validation.passwordValidation(newPassword)) {
      return res.status(422).json({ message: "Set a strong password" });
    }
    const userExist = await User.findOne({ email: email });
    if (userExist) {
      try {
        const OTPExist = await OTP.findOne({ _userId: userExist._id })
        if (OTPExist) {
          if (OTPExist.otp == otp) {
            userExist.password = newPassword;
            userExist.tokens = []
            await userExist.save();
            await OTPExist.remove({});
            return res.status(200).json({ message: "New password set!" });
          } else {
            return res.status(422).json({ error: "invalid OTP" })
          }
        } else {
          return res.status(422).json({ error: "OTP not exist" })
        }
      } catch (err) {
        console.log(err);
        return res.status(500).json({ message: "Internal server error" });
      }
    } else {
      return res.status(422).json({ message: "Email not registered" });
    }
  } else {
    return res.status(422).json({ error: "Method not allowed" });
  }
}
