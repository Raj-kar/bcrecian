require("../DB/conn");
const User = require("../models/userSchema.js");
const OTP = require("../models/otpSchema");
const ObjectId = require("mongoose").Types.ObjectId;
const SendMail = require("../helpers/sendMail");
const bcrypt = require("bcrypt");
const env = require("../config");

export default async function handler(req, res) {
  if (req.method === "POST") {
    const { email } = req.body;
    const userExist = await User.findOne({ email: email });
    if (userExist) {
      try {
        const otpExist = await OTP.findOne({
          _userId: new ObjectId(userExist._id),
        });
        if (otpExist) {
          otpExist.remove({});
        }
        let otpCode = Math.floor(Math.random() * (9999 - 1000) + 1000);
        // console.log(otpCode);
        let link = await bcrypt.hash(otpCode.toString(), 12);
        link = link.replace(".", "");
        link = link.replace("/", "");
        link = link.replace("$", "");
        // console.log(link);
        const emailLink =
          "http://" + req.headers.host + "/api/auth/confirmemail?link=" + link;
        const emailBody = {
          head: "Verify Your account now!",
          body: "Thank you for using College Space .",
        };
        try {
          await SendMail(emailLink, email, userExist.name, emailBody);
        } catch (e) {
          console.log(e);
          return res.status(422).json({ error: "Some error occurred" });
        }
        try {
          let otp = new OTP({ _userId: userExist._id, otp: link });
          await otp.save();
          res
            .status(200)
            .json({ message: "New otp send to the registered email" });
        } catch (err) {
          console.log(err);
          res.status(500).json({ error: "Internal Server error" });
        }
      } catch (err) {
        console.log(err);
        res.status(500).json({ error: "Internal Server error" });
      }
    } else {
      return res.status(422).json({ error: "Email not registered" });
    }
  } else {
    return res.status(422).json({ error: "Method not allowed" });
  }
}
