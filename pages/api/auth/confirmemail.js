require("../DB/conn");
const User = require("../models/userSchema.js");
const OTP = require("../models/otpSchema");
const ObjectId = require("mongoose").Types.ObjectId;

export default async function handler(req, res) {
  const host = req.headers.host
  const { link } = req.query;
  const otpExist = await OTP.findOne({ otp: link });
  if (otpExist) {
    const userExist = await User.findOne({
      _id: new ObjectId(otpExist._userId),
    });

    if (userExist) {
      if (userExist.isVerified) {
        return res.status(200).send(template(host, "ALREADY VERIFIED!!", "Your account is already verified", `Click the "HOME" button below to redirect`, `http://${host}/shake.png`), '/')
      }

      try {
        userExist.isVerified = true;
        await userExist.save();
        // await otpExist.remove({});
        return res.status(200).send(template(host, "VERIFIED!!", "Your account is verified", `Now you can go back or click the "Home" button for doing further processes`, `http://${host}/shake.png`), '/?user=verify')

      } catch (err) {
        console.log(err);
        return res.status(500).send("<h1>Internal Server Error</h1>")
      }
    } else {
      return res.status(200).send(template(host, "SORRY!!", "Your email is not registered!:(", `Click the "Home" button below and complete registration process.`, `http://${host}/exclaim.gif`))
    }
  } else {
    return res.status(200).send(template(host, "OOPS!!", "This link is expired:(", `Click the "Home" button below and apply for resend otp`, `http://${host}/exclaim.gif`))
  }
}





function template(host, title, message, body, img, link = '/') {
  return `<!DOCTYPE html>
  <html lang="en">
  
  <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        body{
            display: flex;
            align-items: center;
            justify-content: center;
            align-content: center;
            text-align: center;
            font-family: Roboto,sans-serif;
        }
        button{
            background: #0079F3;
            margin: 14.4px 0px;
            padding: 7.2px 36px;
            color: white;
            border: none;
            border-radius: 5px;
            text-align: center;
            font-family: Roboto,sans-serif;
            transition: all .3s;
            cursor: pointer;
        }

        button:hover{
            transform: scale(1.02) ;
    box-shadow: 0 .1rem .5rem #333;
    background-image: linear-gradient(to bottom right);
        }
        a{
          text-decoration:none;
          background: #0079F3;
          padding: 7.2px 36px;
          color:white;
          border-radius: 5px;
          text-align: center;
        }
        img{
            border: none;
            border-radius: 50%;
            padding: 30px;
            width: 125px;
            height: 110px;

        }
        .main-page{
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -60%);
            width: 92%;
        }
        H2{
            margin: 0px;
            color: #001E3C;
        }
        h6{
            margin: 0px;
            color:dimgrey;
        }
        @media (max-width:500px) {
          .main-body {
              transform: scale(1.5)
          }
      }

      @media (min-width:500px) {
          .main-body {
              transform: scale(1)
          }
      }
    </style>
</head>
<title>${host}</title>
<body>
    <div class="main-page">
        <div>
            <img src="${img}" alt="handshake">
        </div>
        <div>
            <H1>${title}</h1><br>
            <H2>${message}</H2>
            <h6>${body}</h6><br>
            <a href="${link}" target="child" id="redirect" onclick="redirect">Home</a>       
        </div>
    </div>
    
</body>
<script>
function redirect(){
  window.open('${host}', 'child-window');
}

</script>
</html>`
}