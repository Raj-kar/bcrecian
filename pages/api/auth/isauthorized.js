require("../DB/conn");
const jwt = require("jsonwebtoken");
const Cookies = require("cookies");
const User = require("../models/userSchema");
const env = require("../config");

export default async function handler(req, res) {
  try {
    const cookies = new Cookies(req, res);
    // Get a cookie
    const token = cookies.get("jwt");
    if (token) {
      const verifyUser = jwt.verify(token, env.mpJWTSecretKey);
      console.log(verifyUser);
      const userExist = await User.findOne({ _id: verifyUser._id });
      console.log(userExist.tokens)
      let tokens = userExist.tokens
      let tokenExist = false;
      for (let i = 0; i < tokens.length; i++) {
        if (tokens[i].token == token) {
          tokenExist = true;
          break;
        }
      }

      if (userExist && tokenExist) {
        const userDetails = {
          name: userExist.name,
          roll: userExist.roll,
          email: userExist.email,
          isVerified: userExist.isVerified,
          isAddedDetails: userExist.isAddedDetails,
          course: userExist.course,
          semester: userExist.semester,
          section: userExist.section,
        };
        res.status(200).json({ userDetails: userDetails });
      }
      else {
        res.status(200).json({ userDetails: undefined });
      }
    } else {
      res.status(200).json({ userDetails: undefined });
    }
  } catch (err) {
    console.log(err);
    res.status(422).json({ error: "Invalid cookies" });
  }
}
