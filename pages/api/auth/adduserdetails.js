require("../DB/conn");
const User = require("../models/userSchema.js");
const Validation = require("../validation/validator.js");
const sendMail = require("../helpers/sendMail");
const OTP = require("../models/otpSchema");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const Course = require("../models/courseSchema");
var Cookies = require("cookies");
const env = require("../config");

export default async function handler(req, res) {
  if (req.method === "POST") {
    try {
      const { course, semester, section } = req.body;
      if (!course || !semester || !section) {
        return res.status(422).json({ error: "Fields can't be blank" });
      }
      if (section != "A" && section != "B") {
        return res.status(422).json({ error: "Invalid details provided" });
      }
      const cookies = new Cookies(req, res);
      // Get a cookie
      const token = cookies.get("jwt");
      const verifyUser = jwt.verify(token, env.mpJWTSecretKey);
      console.log(verifyUser);
      const userExist = await User.findOne({ _id: verifyUser._id });

      if (userExist) {
        if (userExist.isVerified) {
          let isValidDetails;
          try {
            isValidDetails = await Course.findOne({
              course: course,
              semesters: { $in: [parseInt(semester)] },
            });
            // console.log(isValidDetails)
          } catch (e) {
            console.log(e);
            return res.status(500).json({ error: "Internal server error" });
          }
          console.log(isValidDetails);
          if (!isValidDetails) {
            return res.status(422).json({ error: "Invalid details provided" });
          }
          console.log(userExist.name);
          userExist.course = course;
          userExist.semester = semester;
          userExist.section = section;
          userExist.isAddedDetails = true;
          userExist.save();
          res.status(200).json({
            message: "Information saved",
            userDetails: {
              name: userExist.name,
              roll: userExist.roll,
              email: userExist.email,
              isVerified: userExist.isVerified,
              isAddedDetails: userExist.isAddedDetails,
              course: userExist.course,
              semester: userExist.semester,
              section: userExist.section,
            },
          });
        } else {
          return res.status(422).json({ error: "User not verified" });
        }
      } else {
        return res.status(422).json({ error: "Something went wrong" });
      }
    } catch (e) {
      console.log(e);
      return res.status(500).json({ error: "User is not authorized" });
    }
  } else {
    return res.status(422).json({ error: "Method not allowed" });
  }
}
