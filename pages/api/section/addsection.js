require('../DB/conn');
const Section = require("../models/sectionSchema")

export default async function handler(req, res) {
    if (req.method === 'POST') {
        const { section } = req.body;
        let addSection = new Section({ sectionName: section });
        try {
            let result = await addSection.save();
            if (result) {
                return res.status(200).json({ message: "Section Added!" })
            }
        } catch {
            return res.status(500).json({ error: "Internal Server Error" });
        }
    } else {
        return res.status(422).json({ error: "Method not allowed" });
    }
}