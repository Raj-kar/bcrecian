require('../DB/conn');
const Course = require("../models/courseSchema")

export default async function handler(req, res) {
    try {
        let courses = await Course.find({});
        if (courses) {
            return res.status(200).json({ courses: courses })
        }
        else {
            return res.status(422).json({ error: "No Course added!" });
        }
    } catch (e) {
        console.log(e)
        return res.status(500).json({ error: "Internal Server Error" });
    }

}