require('../DB/conn');
const Course = require("../models/courseSchema")

export default async function handler(req, res) {
    if (req.method === 'POST') {
        const { course, semesters } = req.body;
        console.log(course)
        console.log(semesters)
        let addCourse = new Course({ courseName: course, semesters: semesters });
        // addCourse.semesters.insertMany(semesters)
        //addCourse.semesters = semesters//addCourse.semesters.concat({ semester: semesters.semester });

        try {
            let result = await addCourse.save();
            if (result) {
                return res.status(200).json({ message: "Course Added!" })
            }
        } catch (e) {
            console.log(e)
            return res.status(500).json({ error: "Internal Server Error" });
        }
    } else {
        return res.status(422).json({ error: "Method not allowed" });
    }
}