const mongoose = require("mongoose");
const env = require("../config");

const DB = env.mpDB;
mongoose
  .connect(DB, {
    useNewUrlParser: true,
    //useCreateIndex: true,
    useUnifiedTopology: true,
    //useFindAndModify: false
  })
  .then(() => {
    console.log("Database Connected");
  })
  .catch((err) => console.log("Database not connected " + err));
