require('../DB/conn');
const Semester = require("../models/semesterSchema")

export default async function handler(req, res) {
    if (req.method === 'POST') {
        const { semester } = req.body;
        let addSemester = new Semester({ semesterName: semester });
        try {
            let result = await addSemester.save();
            if (result) {
                return res.status(200).json({ message: "semester Added!" })
            }
        } catch (e) {
            console.log(e)
            return res.status(500).json({ error: "Internal Server Error" });
        }
    } else {
        return res.status(422).json({ error: "Method not allowed" });
    }
}