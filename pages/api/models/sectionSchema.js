const mongoose = require("mongoose");
const sections = new mongoose.Schema({
    sectionName: {
        type: String,
        required: true
    }
});


let section;
try {
    section = mongoose.model('SECTION')
} catch (error) {
    section = mongoose.model('SECTION', sections);
}
module.exports = section;