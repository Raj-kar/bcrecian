const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const jwt = require('jsonwebtoken');
const userSchema = new mongoose.Schema({
    name: {
        type: String,
        require: true
    },
    roll: {
        type: Number,
        require: true,
        unique: true
    },
    email: {
        type: String,
        require: true,
        unique: true
    },
    password: {
        type: String,
        require: true
    },
    isVerified: {
        type: Boolean,
        default: false
    },
    isAddedDetails: {
        type: Boolean,
        default: false
    },

    tokens: [
        {
            token: {
                type: String,
                required: true, ref: 'users',
                expireAt: { type: Date, default: Date.now, index: { expires: 200 } }
            }
        }
    ],
    course: {
        type: String
    },
    semester: {
        type: String,
    },
    section: {
        type: String,
    },
    notification_tokens: [
        {
            token: {
                ref: 'users',
                required: true,
                type: String,
            },
        }
    ],
    notification_check_time: {
        type: Date
    }

});

// hashing password
userSchema.pre('save', async function (next) {
    if (this.isModified('password')) {
        this.password = await bcrypt.hash(this.password, 12);
    }
    next();
})

// generating auth token
// userSchema.methods.generateAuthToken = async function () {
//     try {
//         const token = jwt.sign({ _id: this._id }, "mynameisaanandagopaldutta");
//         this.tokens = this.tokens.concat({ token: token });
//         await this.save();
//         return token;
//     } catch (err) {
//         console.log(err);
//     }
// }
// const User = mongoose.model("USER", userSchema);

let User;
try {
    User = mongoose.model('USER')
} catch (error) {
    User = mongoose.model('USER', userSchema);
}

module.exports = User;
