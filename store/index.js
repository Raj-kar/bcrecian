import { configureStore } from "@reduxjs/toolkit";
import themeReducer from "./theme";
import userReducer from "./user";

// reducer also can be a object for bigger applications :)
const store = configureStore({
  reducer: { theme: themeReducer, user: userReducer },
});

export default store;
